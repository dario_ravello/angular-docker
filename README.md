# Frontend Challenge

This challenge was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11. It's al CRUD of superheros.


## Prerequisites

- [Node](https://nodejs.org/)
- [AngularCLI](https://angular.io/cli)
- [Docker](https://docs.docker.com/docker-for-windows/install/)

## Getting start

1) Clone the repo

```bash
git clone https://darioravello1989@bitbucket.org/dario_ravello/frontend-challenge.git
```

2) Build/Run the docker in your pc. This process may take a few minutes.
```bash
docker build -t angulardocker .
docker run -d -it -p 80:4200 -p 3000:3000 --name angulardocker angulardocker
go to [App](http://localhost/)
```

## Code Arquitecture
This is the project structure. This is the arquitecutre of an angular multi modules SPA.

```bash
src/
|
|– app/
|   |– core/                                # Core module
|   |   |– authentication/                  # Auth files
|   |   |– guards/                          # Guards (pending)
|   |   |– interceptors/                    # Interceptors 
|   |   |– mocks/                           # Mocks
|   |   |– services/                        # Services
|   |   |– core.module                      # Core Module File
|   |   |– ensureModuleLoadedOnceGuard      # Ensure Module Loaded Once File
|   |
|   |– modules/                             # Modules to scale
|   |   |– home/                            # Home Module
|   |   |   |– components/                  # Home's components
|   |   |   |– models/                      # Home's Models
|   |   |   |– pages/                       # Home's Pages
|   |   |   |– home.module                  # Home Module File
|   |   |   |– home-routing.module          # Home Routing
|   |
|   |– shared/                              # Shared module
|   |   |– components/                      # Shared Components
|   |   |   |– atomic-design/               # Atomic Design
|   |   |   |   |– atoms/                   # Atoms
|   |   |   |   |– molecules/               # Molecules
|   |   |   |   |– organisms/               # Organisms
|   |   |   |   |– templates/               # Templates
|   |   |– directives/                      # Shared directives
|   |   |– layout/                          # Shared layouts
|   |   |   |– desktop/                     # Shared layout for desktop
|   |   |   |– mobile/                      # Shared layout for mobile
|   |   |– models/                          # Shared models
|   |   |– pipes/                           # Shared pipes
|   |   |– shared.module                    # Shared module file
|   |
|   |– app.module                           # App Module File
|   |– app.component                        # App Component
|   |– app-routing.module                   # App Routing
|
|– assets/                                  # Assets
|   |– fonts/                               # Fonts files
|   |– images/                              # Images
|   |– js/                                  # Javascripts files
|   |– scss/                                # Sass files (look below Sass Pattern)
|
|– environments/
|   |– environment.prod.ts                  # Variables for production
|   |– environment.ts                       # Main Variables File
|
– main.ts                                   # Main TS file
– favicon.ico                               # Favicon browser
– index.html                                # Root file
```
 
## Running unit tests

Checking 100% coverage report. The project only accepts 100% coverage parameterized in karma.conf.js.
Path report: ./coverage/frontend/index.html
```bash
ng test --no-watch --code-coverage
```

Run unit tests:
```bash
ng test
```

## Running end-to-end tests (pending)

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Sass Arquitecture
The project uses the [7–1 pattern](https://sass-guidelin.es/#architecture), is a common Sass architecture, and is recommended by the Sass Guidelines Project mixed with the concept of Atomic Design.

```bash
sass/
|
|– helpers/
|   |– _variables.scss    # Sass Variables
|   |– _functions.scss    # Sass Functions
|   |– _mixins.scss       # Sass Mixins
|   |– _placeholders.scss # Sass Placeholders
|   |– helpers.scss       # File importer
|
|– base/
|   |– _reset.scss        # Reset/normalize
|   |– _colors.scss       # Colors variables
|   |– _typography.scss   # Typography rules
|   |– base.scss          # File importer
|
|– components/
|   |– components.scss        # File importer
|   |– atomic-design/         # Atomic design
|       |– atoms/             # Atoms
|       |– molecules/         # Molecules
|       |– organisms/         # Organisms
|       |– templates/         # Templates
|
|– layout/
|   |– _navigation.scss   # Navigation
|   |– _grid.scss         # Grid system
|   |– _header.scss       # Header
|   |– _footer.scss       # Footer
|   |– _sidebar.scss      # Sidebar
|   |– _forms.scss        # Forms
|   |– layout.scss        # File importer
|
|– pages/
|   |– _home.scss        # Home specific styles
|   |– pages.scss        # File importer
|
|– themes/
|   |– _theme.scss        # Default theme
|   |– themes.scss        # File importer 
|
|– vendors/
|   |– _bootstrap.scss    # Bootstrap
|   |– _jquery-ui.scss    # jQuery UI
|   |– vendors.scss       # File importer 
|
– _shame.scss             # Hacks and quick-fixes
– main.scss               # Main Sass file
```

## TODO List

- Use multiples favicons for all devices
- e2e Testing with protractor
- Unit testing for components and services
