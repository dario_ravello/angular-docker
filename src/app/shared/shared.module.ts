import { NgModule } from '@angular/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './layout/desktop/footer/footer.component';
import { HeaderComponent } from './layout/desktop/header/header.component';
import { NotFoundComponent } from './layout/desktop/not-found/not-found.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FilterPipe } from './pipes/filter.pipe';
import { HighlightDirective } from './directives/highlight.directives';
import { TableComponent } from './components/table/table.component';
import { TypeaheadComponent } from './components/typeahead/typeahead.component';
import { UppercaseDirective } from './directives/uppercase.directive';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { InputTextComponent } from './components/input-text/input-text.component';
import { MatButtonModule } from '@angular/material/button';
import { TitleComponent } from './components/title/title.component';

@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    NotFoundComponent,
    FilterPipe,
    HighlightDirective,
    TableComponent,
    TypeaheadComponent,
    UppercaseDirective,
    SpinnerComponent,
    InputTextComponent,
    TitleComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    FontAwesomeModule
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    NotFoundComponent,
    FilterPipe,
    HighlightDirective,
    UppercaseDirective,
    SpinnerComponent,
    InputTextComponent,
    TitleComponent
  ]
})
export class SharedModule {
}
