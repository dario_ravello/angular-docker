import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { WaterJugRiddleService } from 'src/app/core/services/water-jug-riddle.service';
import { MainComponent } from './main.component';

let component: MainComponent;
let fixture: ComponentFixture<MainComponent>;

describe('MainComponent', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [MainComponent],
      providers: [WaterJugRiddleService]
    });
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  it(`bottleX has default value`, () => {
    expect(component.bottleX).toEqual(`5`);
  });

  it(`bottleY has default value`, () => {
    expect(component.bottleY).toEqual(`3`);
  });

  it(`bottleZ has default value`, () => {
    expect(component.bottleZ).toEqual(`4`);
  });

  it(`combinations has default value`, () => {
    expect(component.combinations).toEqual([]);
  });

  describe('onPressCalculate', () => {
    it('makes expected calls', () => {
      const waterJugRiddleServiceStub: WaterJugRiddleService = fixture.debugElement.injector.get(
        WaterJugRiddleService
      );
      spyOn(
        waterJugRiddleServiceStub,
        'calculateWaterJugRiddle'
      ).and.callThrough();
      component.onPressCalculate("5","4","3");
      expect(
        waterJugRiddleServiceStub.calculateWaterJugRiddle
      ).toHaveBeenCalled();
    });
  });

  describe('onPressCalculate 2', () => {
    it('makes expected calls', () => {
      const waterJugRiddleServiceStub: WaterJugRiddleService = fixture.debugElement.injector.get(
        WaterJugRiddleService
      );
      spyOn(
        waterJugRiddleServiceStub,
        'calculateWaterJugRiddle'
      ).and.callThrough();
      component.onPressCalculate("12","4","3");
      expect(
        waterJugRiddleServiceStub.calculateWaterJugRiddle
      ).toHaveBeenCalled();
    });
  });
});
