import { Component, OnInit } from '@angular/core';
import { BottlesState } from 'src/app/core/models/bottles-state.model';
import { WaterJugRiddleService } from 'src/app/core/services/water-jug-riddle.service';

@Component({
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public bottleX: string = "9999";
  public bottleY: string = "11";
  public bottleZ: string = "33";

  public error : string = "";
  public combinations : BottlesState[] = [];

  constructor(private waterJugRiddleService: WaterJugRiddleService) { }

  ngOnInit(): void { }

  onPressCalculate(bottleX: string, bottleY: string, bottleZ: string){
    const result = this.waterJugRiddleService.calculateWaterJugRiddle(Number(bottleX), Number(bottleY), Number(bottleZ));
    if(Array.isArray(result)) {
      this.combinations = result;
      this.error = "";
    } else {
      this.error = result;
      this.combinations = [];
    }
    console.log(result);
  }

  onPressCalculateApi(bottleX: string, bottleY: string, bottleZ: string){
    this.waterJugRiddleService.validateBottles(Number(bottleX), Number(bottleY), Number(bottleZ));
    this.waterJugRiddleService.getWaterBottleRiddle(Number(bottleX), Number(bottleY), Number(bottleZ)).subscribe((data) => {
      if(!data.length) {
        this.error = "No Solution";
      }
      this.combinations = data;
    });
  }
}
