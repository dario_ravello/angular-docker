import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WaterBottleComponent } from './water-bottle.component';

describe('WaterBottleComponent', () => {
  let component: WaterBottleComponent;
  let fixture: ComponentFixture<WaterBottleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WaterBottleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WaterBottleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
