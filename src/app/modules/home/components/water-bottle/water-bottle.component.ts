import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-water-bottle',
  templateUrl: './water-bottle.component.html',
  styleUrls: ['./water-bottle.component.scss']
})
export class WaterBottleComponent implements OnInit {

  @Input()
  fillValue!: number;

  @Input()
  stateValue!: string;

  constructor() { }

  ngOnInit(): void {
  }

}
