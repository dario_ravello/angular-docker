import { TestBed } from '@angular/core/testing';
import { BottlesState } from '../models/bottles-state.model';
import { WaterJugRiddleService } from './water-jug-riddle.service';

describe('WaterJugRiddleService', () => {
  let service: WaterJugRiddleService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [WaterJugRiddleService] });
    service = TestBed.inject(WaterJugRiddleService);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });

  it(`bottleXMax has default value`, () => {
    expect(service.bottleXMax).toEqual(5);
  });

  it(`bottleYMax has default value`, () => {
    expect(service.bottleYMax).toEqual(3);
  });

  it(`check calculateWaterJugRiddle`, () => {
    expect(service.calculateWaterJugRiddle(5,3,4)).toEqual([{"bottleX":0,"bottleXState":"Empty","bottleY":0,"bottleYState":"Empty"},{"bottleX":0,"bottleXState":"Empty","bottleY":4,"bottleYState":"Full"},{"bottleX":5,"bottleXState":"Full","bottleY":0,"bottleYState":"Empty"},{"bottleX":5,"bottleXState":"Full","bottleY":4,"bottleYState":"Full"},{"bottleX":4,"bottleXState":"Partial Full","bottleY":0,"bottleYState":"Empty"},{"bottleX":1,"bottleXState":"Partial Full","bottleY":4,"bottleYState":"Full"},{"bottleX":4,"bottleXState":"Partial Full","bottleY":4,"bottleYState":"Full"},{"bottleX":5,"bottleXState":"Full","bottleY":3,"bottleYState":"Partial Full"},{"bottleX":0,"bottleXState":"Empty","bottleY":3,"bottleYState":"Partial Full"}]);
  });

  it(`check calculateWaterJugRiddle Error 1`, () => {
    expect(service.calculateWaterJugRiddle(0,3,4)).toEqual("You need to fill in the quantity of the bottle X");
  });

  it(`check calculateWaterJugRiddle Error 2`, () => {
    expect(service.calculateWaterJugRiddle(5,0,4)).toEqual("You need to fill in the quantity of the bottle Y");
  });

  it(`check calculateWaterJugRiddle Error 3`, () => {
    expect(service.calculateWaterJugRiddle(5,3,0)).toEqual("You need to fill in the quantity of the bottle Z");
  });

  it(`check calculateWaterJugRiddle Error 7`, () => {
    expect(service.calculateWaterJugRiddle(1234,3,4)).toEqual("The quantity of the bottle X must be between 0 and 99");
  });

  it(`check calculateWaterJugRiddle Error 8`, () => {
    expect(service.calculateWaterJugRiddle(5,123,4)).toEqual("The quantity of the bottle Y must be between 0 and 99");
  });

  it(`check calculateWaterJugRiddle Error 9`, () => {
    expect(service.calculateWaterJugRiddle(5,3,123)).toEqual("The quantity of the bottle Z must be between 0 and 99");
  });

  it(`check calculateWaterJugRiddle Error 10`, () => {
    expect(service.calculateWaterJugRiddle(1,3,5)).toEqual("The quantity of the bottle z must be greater than those of X and Y");
  });

});
