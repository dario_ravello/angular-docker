export class BottlesState {
  bottleX: number = 0;
  bottleXState: string = "Empty";
  bottleY: number = 0;
  bottleYState: string = "Empty";
  action: string = "Start";
}
