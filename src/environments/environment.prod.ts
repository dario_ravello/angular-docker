const apiEndpoint = 'http://localhost:5000';

export const environment = {
  production: true,
  apiEndpoint: apiEndpoint,
  api: {
    getWaterBottleRiddle: `${apiEndpoint}/api/waterbottle/:bottleX/:bottleY/:bottleZ`
  }
};
